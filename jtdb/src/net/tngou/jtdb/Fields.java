package net.tngou.jtdb;

import java.util.ArrayList;
import java.util.List;


/**
 * 
* @ClassName: Fields
* @Description: TODO(这里用一句话描述这个类的作用)
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年5月21日 下午3:41:38
*
 */
public class Fields {

   private	List<Field> list = new ArrayList<Field>();
	
   
   
	public void add(Field field) {
		list.add(field);
	}
   
   
	@Override
	public String toString() {
		String s="";
		
		for (Field e : list) {
			s=s+"name:"+e.getName()+" "
					+"value:"+e.getValue()+" "
					+"type:"+e.getType()+"\n";
		}
	

		return s;
	}


	public List<Field> getList() {
		return list;
	}


	public void setList(List<Field> list) {
		this.list = list;
	}
	

}
